import { useRef } from "react"

function UseRefComponent(){
  
  const inputRef = useRef<HTMLInputElement | null>(null);
  
  return(
    <div>
      <input ref={inputRef}></input>
    </div>
  )
}

export default UseRefComponent