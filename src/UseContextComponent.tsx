import UserContext, {UserState} from './store';
import { useContext, useState } from 'react';

function ConsumerComponent(){
  const user = useContext(UserContext);
  return(
    <div>
      <div>First: {user.firstName} </div>
      <div>Last: {user.lastName} </div>
    </div>
  )
}

function UserContextComponent(){
  const [user, userSet] = useState<UserState>({
    firstName: "Mehdi",
    lastName:"Jamai"
  });
  return(
    <UserContext.Provider value={user}>
      <ConsumerComponent />
        <button onClick={() => userSet({
          firstName: "Debi",
          lastName:"Nephew"
        })}>Change User Context</button>
    </UserContext.Provider>
  )
}

export default UserContextComponent

