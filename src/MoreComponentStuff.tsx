import { ReactElement, ReactNode } from "react";

export interface HeadingProps{
  title:string
}

export function Heading({title}:HeadingProps){
  return(
    <p>{title}</p>
  )
}

export type ListComponent = <ListItem>({items,render}:{
  items:ListItem[];
  render:(item:ListItem)=> ReactNode;
}) => ReactElement

export const List: ListComponent = ({ items, render }) => {
  return (
    <ul>
      {items.map((item, index) => (
        <li key={index}>{render(item)}</li>
      ))}
    </ul>
  )
};

function MoreComponentStuff(){
  return(
    <div>
      <Heading title="Hello"/>
      <List items={["a", "b", "c"]} render={(str)=> <strong>{str}</strong>}></List>
    </div>
  )
}
export default MoreComponentStuff